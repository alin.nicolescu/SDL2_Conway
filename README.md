# SDL2 Conway's Game of Life

![Screenshot](conway.png)

The `*.h` files are *single file header libraries* for `C`.

References:

- [https://www.libsdl.org/](https://www.libsdl.org/)
- [https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
- [Single file library howto](https://github.com/nothings/stb/blob/master/docs/stb_howto.txt)
- [https://github.com/nothings/stb](https://github.com/nothings/stb)

## Usage

- Press `q` to quit
- Use a `left` mouse click to *select/deselect* a cell
- Press `s` to *start/stop* the game
- Press `r` to *reset* the game
- Press `a` to *add* random viruses
- Press `i` to *add* an empty grid line
- Press `o` to *add* an empty grid column

## Build and run

Tested on:

- `Linux` (Debian and Arch)
- `FreeBSD`
- `Windows 10`

### Linux and FreeBSD

```console
make && ./conway
```

### Windows

Use the `winmake.bat` file.

The app compiles fine with [MinGW](https://sourceforge.net/projects/mingw/)
assuming you set the *path environment variable*:

```
mingw32-make.exe -f WinMakefile
```

Change the following lines from `WinMakefile`  if you installed `SDL2` in
other location:

```
INCLUDE_PATH = C:\MinGW\mingw64\include
LIBS_PATH = C:\MinGW\mingw64\libs
```

