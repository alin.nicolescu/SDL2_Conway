#ifndef SDL2_GRID_H
#define SDL2_GRID_H

#include<stdbool.h>
#include<SDL2/SDL.h>
#include "utils.h"

#define MAX_GRID_SIZE 100

typedef struct {
    SDL_Point corner; // upper left corner
    size_t cols;
    size_t rows;
    size_t cell_width; // in pixels
    size_t cell_heigth; // in pixels
    size_t offset; // spacing between cells
    SDL_Color color;
} grid_t;

extern SDL_Renderer* renderer;

extern void InitGrid(grid_t* g,size_t w,size_t h,size_t cw, size_t ch,const SDL_Color* color);
extern void SetOffset(grid_t* g,size_t offset);
extern void SetCorner(grid_t* g,SDL_Point* corner);
extern size_t GridPixelWidth(const grid_t* g);
extern size_t GridPixelHeigth(const grid_t* g);
extern void DrawGrid(const grid_t* g);
extern void GetCellCorner(const grid_t* g,size_t row,size_t col,SDL_Point* corner);
extern bool GetCell(const grid_t* g,int x,int y,size_t* row,size_t* col);
extern void FillCell(const grid_t* g,size_t row,size_t col,const SDL_Color* c);
extern void XCell(const grid_t* g,size_t row,size_t col,const SDL_Color* c);

#ifdef SDL2_GRID_IMPLEMENTATION

void InitGrid(grid_t* g,size_t w,size_t h,
        size_t cw, size_t ch,const SDL_Color* color)
{
    if(w==0 || h==0 || w>MAX_GRID_SIZE || h>MAX_GRID_SIZE)
    {
        fprintf(stderr,"[ERROR] InitGrid: bad grid size\n");
        exit(EXIT_FAILURE);
    }

    g->cols = w;
    g->rows = h;
    g->cell_width = cw;
    g->cell_heigth = ch;
    g->color = *color;
}

void SetOffset(grid_t* g,size_t offset)
{
    g->offset = offset;
}

void SetCorner(grid_t* g,SDL_Point* corner)
{
    g->corner = *corner;
}

size_t GridPixelWidth(const grid_t* g)
{
    return g->cols * g->cell_width;
}

size_t GridPixelHeigth(const grid_t* g)
{
    return g->rows * g->cell_heigth;
}

void DrawGrid(const grid_t* g)
{
    size_t grid_right_x = g->corner.x + g->cell_width * g->cols;
    size_t grid_bottom_y = g->corner.y + g->cell_heigth * g->rows;

    SDL_SetRenderDrawColor(renderer,g->color.r,g->color.g,g->color.b,SDL_ALPHA_OPAQUE);

    for(size_t i = 0;i <= g->rows;++i)
    {
        int y = g->corner.y + i*g->cell_heigth;
        SDL_RenderDrawLine(renderer,g->corner.x,y,grid_right_x,y);
    }

    for(size_t i = 0;i <= g->cols;++i)
    {
        int x = g->corner.x + i*g->cell_width;
        SDL_RenderDrawLine(renderer,x,g->corner.y,x,grid_bottom_y);
    }
}

void GetCellCorner(const grid_t* g,size_t row,size_t col,SDL_Point* nw)
{
    nw->x = g->corner.x + col*g->cell_width;
    nw->y = g->corner.y + row*g->cell_heigth;
}

bool GetCell(const grid_t* g,int x,int y,size_t* row,size_t* col)
{
    if(!BETWEEN(x,g->corner.x + 1,g->corner.x + GridPixelWidth(g)) ||
       !BETWEEN(y,g->corner.y + 1,g->corner.y + GridPixelHeigth(g)))
            return false;
    *row = (y - g->corner.y) / (g->cell_heigth);
    *col = (x - g->corner.x) / (g->cell_width);
    return true;
}

void FillCell(const grid_t* g,size_t row,size_t col,const SDL_Color* c)
{
    if(row >= g->rows || col >= g->cols) return;
    SDL_Point P;
    GetCellCorner(g,row,col,&P);
    SDL_Rect r = {
        .x=P.x+g->offset,
        .y=P.y+g->offset,
        .w=g->cell_width-2*g->offset,
        .h=g->cell_heigth-2*g->offset};
    SDL_SetRenderDrawColor(renderer,c->r,c->g,c->b,SDL_ALPHA_OPAQUE);
    SDL_RenderFillRect(renderer,&r);
    /* SDL_RenderDrawRect(renderer,&r); */
}

void XCell(const grid_t* g,size_t row,size_t col,const SDL_Color* c)
{
    if(row >= g->rows || col >= g->cols) return;
    SDL_Point P;
    GetCellCorner(g,row,col,&P);
    SDL_Rect r = {
        .x=P.x+g->offset,
        .y=P.y+g->offset,
        .w=g->cell_width-2*g->offset,
        .h=g->cell_heigth-2*g->offset};
    SDL_SetRenderDrawColor(renderer,c->r,c->g,c->b,SDL_ALPHA_OPAQUE);
    SDL_RenderDrawLine(renderer,r.x,r.y,r.x+r.w,r.y+r.h);
    SDL_RenderDrawLine(renderer,r.x+r.w,r.y,r.x,r.y+r.h);
}

#endif //SDL2_GRID_IMPLEMENTATION

#endif //SDL2_GRID_H
