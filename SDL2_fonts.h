#ifndef SDL2_FONTS_H
#define SDL2_FONTS_H

#include<stdio.h>
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>

#define MAX_FONT_NAME_LEN 255

typedef struct{
    TTF_Font* font;
    char name[MAX_FONT_NAME_LEN];
    size_t size;
    SDL_Surface* surface;
    SDL_Texture* texture;
    int  width;
    int heigth;
} font_t;

extern SDL_Renderer* renderer;

extern void FontInit(font_t* f,char* name,size_t size);
extern void FontSetText(font_t* f,char* text,const SDL_Color* color);
extern void FontWrite(font_t* f,int x,int y);
extern void FontCleanup(font_t* f);
extern void CloseFonts();

#ifdef SDL2_FONTS_IMPLEMENTATION

void FontInit(font_t* f,char* name,size_t size)
{
    if(!TTF_WasInit())
    {
        if(TTF_Init() != 0)
        {
            fprintf(stderr,"[ERROR] TTF_Init: %s\n",TTF_GetError());
            exit(EXIT_FAILURE);
        }
    }

   if(strlen(name) >= MAX_FONT_NAME_LEN)
   {
        fprintf(stderr,"[ERROR] Font name too long\n");
        exit(EXIT_FAILURE);
   }
   else
       strcpy(f->name,name);

   f->size = size;

   f->font = TTF_OpenFont(f->name,f->size);
   if(!f->font)
   {
        fprintf(stderr,"[ERROR] TTF_OpenFont: %s\n",TTF_GetError());
        exit(EXIT_FAILURE);
   }

   f->surface = NULL;
   f->texture = NULL;

   f->width = f->heigth = 0;
}

void FontSetText(font_t* f,char* text,const SDL_Color* color)
{
    f->surface = TTF_RenderText_Blended(f->font,text,*color);
    if(!f->surface)
    {
        fprintf(stderr,"[ERROR] TTF_RenderText_Blended: %s\n",TTF_GetError());
        exit(EXIT_FAILURE);
    }
    f->texture = SDL_CreateTextureFromSurface(renderer,f->surface);
    if(!f->texture)
    {
        fprintf(stderr,"[ERROR] SDL_CreateTextureFromSurface: %s\n",SDL_GetError());
        exit(EXIT_FAILURE);
    }

    if(SDL_QueryTexture(f->texture, NULL, NULL, &f->width, &f->heigth) != 0)
    {
        fprintf(stderr,"[ERROR] SDL_QueryTexture: %s\n",SDL_GetError());
        exit(EXIT_FAILURE);
    }
}

void FontWrite(font_t* f,int x,int y)
{
    SDL_Rect destination = { x, y, f->width, f->heigth };
    SDL_RenderCopy(renderer, f->texture, NULL, &destination);
	SDL_DestroyTexture(f->texture);
	f->texture = NULL;
	SDL_FreeSurface(f->surface);
	f->surface = NULL;	
}

void FontCleanup(font_t* f)
{
    TTF_CloseFont(f->font);
    f->font = NULL;
    SDL_DestroyTexture(f->texture);
    SDL_FreeSurface(f->surface);
}

void CloseFonts()
{
    if(TTF_WasInit()) TTF_Quit();
}

#endif //SDL2_FONTS_IMPLEMENTATION

#endif //SDL2_FONTS_H
