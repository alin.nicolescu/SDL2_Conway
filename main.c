#include "utils.h"
#include "SDL2_template.h"
#include "SDL2_fonts.h"
#include "SDL2_grid.h"

#define MIN_SQUARE_SIZE 10
#define MAX_ROWS 50
#define MAX_COLS 50
#define ROWS 10
#define COLS 10
#define GRID_OFFSET 1
#define OFFSET 2
#define LIVE true
#define DEAD false
#define DELAY 500
#define FONT_SIZE 24
#define STRING_MAX_LEN 255

grid_t g;
int rows = ROWS;
int cols = COLS;
font_t f;
bool gen1[MAX_ROWS][MAX_COLS];
bool gen2[MAX_ROWS][MAX_COLS];
size_t generation;
size_t viruses;
bool play = false;
bool allow_cell_toggle = true;
char status[STRING_MAX_LEN];

size_t LiveNeighbours(int row,int col);
void NextGeneration();
void SetStatus();
void Restart();
void RandomViruses();
bool Identical(bool gen1[MAX_ROWS][MAX_COLS],bool gen2[MAX_ROWS][MAX_COLS]);

int main(int argc,char* argv[])
{
    (void) argc;
    (void) argv;

    SetWidthHeigth(800,600);
    SetTitle("Conway's Game of Life");
    SetBackground(&black);

    InitGraph();
    atexit(CloseGraph);

    SetOffset(&g,OFFSET);

    FontInit(&f,"font.ttf",FONT_SIZE);
    atexit(CloseFonts);

    Loop();

    FontCleanup(&f);

    return 0;
}

// SDL2 Template functions definitions

void HandleKeyPress(SDL_Event e)
{
    if(e.key.keysym.sym == SDLK_s)
    {
        if(viruses == 0) return;
        if(!play)
        {
            if(allow_cell_toggle)
                allow_cell_toggle = false;
            play = true;
        }
        else
            play = false;
    }
    else
    if(e.key.keysym.sym == SDLK_r)
    {
            Restart();
    }
    else
    if(e.key.keysym.sym == SDLK_a)
    {
            RandomViruses();
    }
    else
    if(e.key.keysym.sym == SDLK_i)
    {
            if(allow_cell_toggle && rows+1<MAX_ROWS && g.cell_width>MIN_SQUARE_SIZE)
            {
                ++rows;
            }
    }
    else
    if(e.key.keysym.sym == SDLK_o)
    {
            if(allow_cell_toggle && cols+1<MAX_COLS && g.cell_width>MIN_SQUARE_SIZE)
            {
                ++cols;
            }
    }
}

void HandleMouse(SDL_Event e)
{
    if(!allow_cell_toggle) return;

    int x,y;
    size_t row,col;
    if(e.button.button == SDL_BUTTON_LEFT)
    {
        SDL_GetMouseState(&x,&y);
        if(GetCell(&g,x,y,&row,&col))
        {
            gen1[row][col] = !gen1[row][col];
        }
    }
}

void Draw()
{
    viruses=0;
    size_t w = (width-GRID_OFFSET)/cols;
    size_t h = (heigth-2*GRID_OFFSET-f.heigth-2*OFFSET)/rows;
    size_t min = w<h? w:h;
    InitGrid(&g,cols,rows,min,min,&maroon);
    SDL_Point P = {
        .x = (width-GridPixelWidth(&g))/2,
        .y = (heigth-GridPixelHeigth(&g)-f.heigth-2*OFFSET)/2};
    SetCorner(&g,&P);
    DrawGrid(&g);

    for(int i=0;i<rows;++i)
        for(int j=0;j<cols;++j)
        {
            if(gen1[i][j])
            {
                ++viruses;
                FillCell(&g,i,j,&green);
            }
            else
                FillCell(&g,i,j,&brown);
        }

    SetStatus();

    if(play)
    {
        NextGeneration();
        ++generation;
        SDL_Delay(DELAY);
    }
}

// App specific functions

size_t LiveNeighbours(int row,int col)
{
    int dx[]={-1,-1, 0,+1,+1,+1, 0,-1};
    int dy[]={ 0,+1,+1,+1, 0,-1,-1,-1};

    int dir_nr = sizeof(dx)/sizeof(int);

    size_t n = 0;

    for(int i=0;i<dir_nr;++i)
    {
        if(row+dx[i]>=0 && row+dx[i]<rows && 
                col+dy[i]>=0 && row+dy[i]<cols &&
                gen1[row+dx[i]][col+dy[i]]==LIVE)
            ++n;
    }

    return n;
}

void NextGeneration()
{
    for(int i=0;i<rows;++i)
        for(int j=0;j<cols;++j)
        {
            size_t n = LiveNeighbours(i,j);
            if(gen1[i][j]==LIVE)
            {
                if(n==2 || n==3)
                    gen2[i][j] = LIVE;
                else
                    gen2[i][j] = DEAD;
            }
            else
            {
                if(n==3)
                    gen2[i][j] = LIVE;
                else
                    gen2[i][j] = DEAD;
            }

        }
    if(Identical(gen1,gen2))
    {
        play = false;
    }
    else
    {
        for(int i=0;i<rows;++i)
            for(int j=0;j<cols;++j)
                gen1[i][j]=gen2[i][j];
    }
}

void SetStatus()
{
    char s[STRING_MAX_LEN];
    strcpy(status,"");
    UnsignedToString(rows,s,STRING_MAX_LEN);
    strcat(status,s);
    strcat(status,"x");
    UnsignedToString(cols,s,STRING_MAX_LEN);
    strcat(status,s);
    strcat(status," | ");
    UnsignedToString(viruses,s,STRING_MAX_LEN);
    strcat(status,s);
    strcat(status," viruses");
    if(play)
        strcat(status," | Game on");
    else
        strcat(status," | Game paused");
    strcat(status," | Generation: ");
    UnsignedToString(generation,s,STRING_MAX_LEN);
    strcat(status,s);
    
    FontSetText(&f,status,&white);
    FontWrite(&f,(width - f.width)/2,heigth - f.heigth - OFFSET);
}

void Restart()
{
    play = false;
    generation = 0;
    for(int i=0;i<rows;++i)
        for(int j=0;j<cols;++j)
            gen1[i][j]=false;
    allow_cell_toggle = true;
}

void RandomViruses()
{
    if(!allow_cell_toggle) return;

    for(int i=1;i<=rows*cols/4;++i)
    {
        gen1[RANDOM(0,rows-1)][RANDOM(0,cols-1)] = LIVE;
    }
}

bool Identical(bool gen1[MAX_ROWS][MAX_COLS],bool gen2[MAX_ROWS][MAX_COLS])
{
    for(int i=0;i<rows;i++)
        for(int j=0;j<cols;j++)
            if(gen1[i][j]!=gen2[i][j])
                return false;
    return true;

}
